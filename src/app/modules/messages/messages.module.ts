import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpModule } from '@angular/http';

// COMPONENTS
import { RecibedComponent } from './components/recibed/recibed.component';
import { MainComponent } from './main/main.component';

// ROUTING
import { MessagesRoutingModule } from './messages-routing.module';


@NgModule({
  imports: [
    CommonModule,
    MessagesRoutingModule,
    HttpModule
  ],
  declarations: [
    RecibedComponent,
    MainComponent
  ],
  exports: [
    CommonModule,
    MessagesRoutingModule,
    HttpModule
  ]
})
export class MessagesModule { }
