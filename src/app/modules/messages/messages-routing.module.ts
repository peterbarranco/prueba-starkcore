import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MainComponent } from './main/main.component';
import { RecibedComponent } from './components/recibed/recibed.component';

const messagesRoutes: Routes = [
    {
        path: 'messages', component: MainComponent,
        children: [
            {path: '', redirectTo: 'recibed', pathMatch: 'full'},
            {path: 'recibed', component: RecibedComponent},
            {path: '**', pathMatch: 'full', redirectTo: 'recibed' }
        ]
    }
];

@NgModule({
    imports: [
        RouterModule.forChild(messagesRoutes)
    ],
    exports: [
        RouterModule
    ]
})
export class MessagesRoutingModule {}

