export interface Delivery {
    date: {
        dayOfWeek: string,
        dayOfMonth: number,
        time: string,
        period: string
    };
    address: {
        origin: string,
        destination: string
    };
    price: number;
    quantity: number;

}


