export const deliveries = [
    {
        'date': {
            'dayOfWeek': 'Monday',
            'dayOfMonth': 10,
            'time': '2:28',
            'period': 'PM'
        },
        'address': {
            'origin': 'Houstong, TX, 33619',
            'destination': 'Atlanta, GA, 30123'
        },
        'price': 250,
        'quantity': 1
    },
    {
        'date': {
            'dayOfWeek': 'Tuesday',
            'dayOfMonth': 17,
            'time': '8:28',
            'period': 'PM'
        },
        'address': {
            'origin': 'Miami, FL, 33619',
            'destination': 'Fort Lauderdale, FL, 30123'
        },
        'price': 150,
        'quantity': 2
    },
    {
        'date': {
            'dayOfWeek': 'Sunday',
            'dayOfMonth': 30,
            'time': '7:08',
            'period': 'AM'
        },
        'address': {
            'origin': 'Oklahoma City, OK, 33619',
            'destination': 'Portlant, OR, 30123'
        },
        'price': 400,
        'quantity': 5
    }
];
