import { Component, OnInit } from '@angular/core';
import { Http } from '@angular/http';
import { deliveries } from '../../data/deliveries';
import { Delivery } from '../../interfaces/delivery.interface';




@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  public deliveries: Delivery[] = [];
  constructor(private http: Http) {
    this.deliveries = deliveries.slice(0);

  }

  ngOnInit() {
  }




}
