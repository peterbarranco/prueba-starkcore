import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';
import { RouterModule } from '@angular/router';

// RUTAS
import { APP_ROUTES } from './app.routes';

// MODULOS CUSTON
import { MessagesModule } from './modules/messages/messages.module';


// COMPONENTS
import { AppComponent } from './app.component';
import { SidebarComponent } from './components/sidebar/sidebar.component';
import { HeaderComponent } from './components/header/header.component';
import { HomeComponent } from './components/home/home.component';





@NgModule({
  declarations: [
    AppComponent,
    SidebarComponent,
    HeaderComponent,
    HomeComponent
  ],
  imports: [
    BrowserModule,
    HttpModule,
    MessagesModule,
    RouterModule.forRoot(APP_ROUTES, {useHash : true})
  ],
  providers: [],
  bootstrap: [AppComponent],
  exports: [
    SidebarComponent,
    HeaderComponent
  ]
})
export class AppModule { }
